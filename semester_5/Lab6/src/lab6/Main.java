package lab6;

import java.nio.file.Path;
import java.nio.file.Paths;


public class Main {

    public static void main(String[] args) {
        Path currentPath = Paths.get(System.getProperty("user.dir"));
        FileExplorer fileExplorerProgram = new FileExplorer(currentPath);
        fileExplorerProgram.run();
    }
}
