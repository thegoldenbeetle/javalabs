package lab6;

import javax.management.RuntimeErrorException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;

public class FileExplorer {

    private static final int MAX_COMMAND_LENGTH = 2;

    private Path currentPath;
    private final Scanner scanner;
    private String[] commandArgs;

    public FileExplorer(Path currentPath) {
        this.currentPath = currentPath;
        scanner = new Scanner(System.in);
    }

    public void run() {
        boolean exitFlag = true;
        while (exitFlag) {
            System.out.print(currentPath + ">");
            try {
                readCommandArgs();
                String commandName = commandArgs[0];
                switch (commandName) {
                    case "ls" -> lsCommand();
                    case "cd" -> cdCommand();
                    case "touch" -> createNewFileCommand();
                    case "rm" -> deleteFileCommand();
                    case "show" -> showFileContentCommand();
                    case "write" -> writeToFileCommand();
                    case "help" -> showHelpCommand();
                    case "exit" -> exitFlag = false;
                    default -> System.out.println("No such command");
                }
            } catch (NoSuchFileException e) {
                System.out.println("No such file or directory!");
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("No command argument!");
            } catch (IllegalArgumentException | IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void readCommandArgs() throws IllegalArgumentException {
        commandArgs = scanner.nextLine().split("\\s+");
        if (commandArgs.length > MAX_COMMAND_LENGTH) {
            throw new IllegalArgumentException("Wrong command usage");
        }
    }

    private void lsCommand() throws IllegalArgumentException {
        if (commandArgs.length == 1) {
            Arrays.stream((new File(currentPath.toString())).listFiles()).forEach(System.out::println);
        } else {
            throw new IllegalArgumentException("Wrong command usage");
        }
    }

    private void cdCommand() throws IllegalArgumentException, IOException {
        File dir = new File((currentPath.resolve(commandArgs[1]).toRealPath()).toString());
        if (!dir.isDirectory()) {
            throw new IllegalArgumentException("Wrong command usage");
        }
        currentPath = dir.toPath();
    }

    private void createNewFileCommand() throws IllegalArgumentException, IOException {
        File file = new File((currentPath.resolve(commandArgs[1])).toString());
        if (!file.createNewFile()) {
            throw new IllegalArgumentException("File already exists!");
        }
    }

    private void deleteFileCommand() throws IOException {
        File file = new File(currentPath.resolve(commandArgs[1]).toRealPath().toString());
        if (!file.isFile()) {
            throw new IllegalArgumentException("Wrong command usage");
        }
        Files.delete(file.toPath());
    }

    private void showFileContentCommand() throws IOException {
        File file = new File(currentPath.resolve(commandArgs[1]).toRealPath().toString());
        if (!file.isFile()) {
            throw new IllegalArgumentException("Wrong command usage");
        }
        Files.lines(file.toPath(), StandardCharsets.UTF_8).forEach(System.out::println);
    }

    private void writeToFileCommand() throws IOException {
        File file = new File(currentPath.resolve(commandArgs[1]).toRealPath().toString());
        if (!file.isFile()) {
            throw new IllegalArgumentException("Wrong command usage");
        }
        String enteredString = scanner.nextLine();
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream((file.toPath()).toString(), true), StandardCharsets.UTF_8))) {
            writer.write(enteredString);
            writer.newLine();
        }
    }

    private void showHelpCommand() {
        System.out.println("Program usage: ");
        System.out.println("ls - show all files and directories in current directory, no arguments");
        System.out.println("cd directory - goes to the specified directory");
        System.out.println("touch filename - create empty file with specified filename, path to file should exists");
        System.out.println("rm filename - remove file with specified filename, file should exists");
        System.out.println("show filename - show specified file content");
        System.out.println("write filename - append new line to specified file, file should exists");
        System.out.println("help - print help, no arguments");
        System.out.println("exit - exit the program, no arguments");
    }

}
