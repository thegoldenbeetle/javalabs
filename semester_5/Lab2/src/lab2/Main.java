package lab2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try {
            int[][] inputMatrix = matrixInput("input.txt");
            System.out.println("Input matrix: ");
            printMatrix(inputMatrix);
            int rowIndex = getRowIndexBasedOnTask(inputMatrix);
            System.out.println(rowIndex);
            inputMatrix = deleteRow(inputMatrix, rowIndex);
            System.out.println();
            System.out.println("Result matrix:");
            printMatrix(inputMatrix);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static int[][] matrixInput(String filename) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(filename));
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int[][] a = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = scanner.nextInt();
            }
        }
        return a;
    }

    public static void printMatrix(int[][] matrix) {
        System.out.print('[');
        int i = 0;
        for (; i < matrix.length - 1; i++) {
            System.out.print(Arrays.toString(matrix[i]));
            System.out.println(',');
        }
        if (matrix.length > i) {
            System.out.print(Arrays.toString(matrix[i]));
        }
        System.out.println(']');
    }

    public static int getRowIndexBasedOnTask(int[][] matrix) {
        int result = 0;
        int minAmongMaxes = matrix[0][0];
        for (int i = 0; i < matrix.length; i++) {
            int maxInCurrentRow = matrix[i][0];
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] >= maxInCurrentRow) {
                    maxInCurrentRow = matrix[i][j];
                }
            }
            if (maxInCurrentRow <= minAmongMaxes) {
                minAmongMaxes = maxInCurrentRow;
                result = i;
            }
        }
        return result;
    }

    public static int[][] deleteRow(int[][] matrix, int row) {
        int[][] result = new int[matrix.length - 1][matrix[0].length];
        System.arraycopy(matrix, 0, result, 0, row);
        System.arraycopy(matrix, row + 1, result, row, matrix.length - row - 1);
        return result;
    }

}
