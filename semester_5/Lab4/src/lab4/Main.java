package lab4;

public class Main {

    public static void main(String[] args) {
        try {
            Triangle triangle = new Triangle(
                    new Point(-4.0, -1.0), new Point(0.0, -3.0), new Point(2.0, 1.0)
            );
            System.out.println(triangle);
            System.out.format("Perimeter is %.5f%n", triangle.getPerimeter());
            System.out.format("Square is %.5f%n", triangle.getSquare());
            Point medianIntersecPoint = triangle.getMedianIntersectionPoint();
            System.out.format("Median intersection point: is (%.5f, %.5f)%n", medianIntersecPoint.x, medianIntersecPoint.y);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
