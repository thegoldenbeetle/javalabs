package lab4;

public class Triangle {

    private final Point a;
    private final Point b;
    private final Point c;

    private final double abLength;
    private final double acLength;
    private final double bcLength;

    public Triangle(Point a, Point b, Point c) throws IllegalArgumentException {
        this.a = a;
        this.b = b;
        this.c = c;
        abLength = Geometry.pointsDistance(a, b);
        acLength = Geometry.pointsDistance(a, c);
        bcLength = Geometry.pointsDistance(b, c);
        if ((abLength + acLength <= bcLength) || (abLength + bcLength <= acLength) || (bcLength + acLength <= abLength)) {
            throw new IllegalArgumentException("There is no triangle");
        }
    }

    public double getSquare() {
        double halfPerimeter = (abLength + bcLength + acLength) / 2;
        return Math.sqrt(
                halfPerimeter * (halfPerimeter - abLength) * (halfPerimeter - acLength) * (halfPerimeter - bcLength)
        );
    }

    public double getPerimeter() {
        return abLength + acLength + bcLength;
    }

    public Point getMedianIntersectionPoint() {
        // (xa1, ya1) - middle of bc
        double ya1 = (b.y + c.y) / 2;
        double xa1 = (b.x + c.x) / 2;
        double xm = (a.x + 2 * xa1) / 3;
        double ym = (a.y + 2 * ya1) / 3;
        return new Point(xm, ym);
    }

    public String toString() {
        return "Triangle with points A" + a + ", B" + b + ", C" + c;
    }

    public Point getA() {
        return a;
    }

    public Point getB() {
        return b;
    }

    public Point getC() {
        return c;
    }

    public double getLengthAB() {
        return abLength;
    }

    public double getLengthBC() {
        return bcLength;
    }

    public double getLengthAC() {
        return acLength;
    }
}
