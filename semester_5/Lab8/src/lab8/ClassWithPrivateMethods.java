package lab8;

public class ClassWithPrivateMethods {

    @CallNumber(number = 1)
    private void method1() {
        System.out.println("method 1");
    }

    @CallNumber(number = 4)
    private void method2() {
        System.out.println("method 2");
    }

    private void method3() {
        System.out.println("method 3");
    }
}
