package lab8;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {

    public static void main(String[] args) {

	    ClassWithPrivateMethods classPrivate = new ClassWithPrivateMethods();

        for (Method method : classPrivate.getClass().getDeclaredMethods()) {
            CallNumber annotation = method.getAnnotation(CallNumber.class);
            if (annotation != null) {
                method.setAccessible(true);
                for (int i = 0; i < annotation.number(); ++i) {
                    try {
                        method.invoke(classPrivate);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }
}
