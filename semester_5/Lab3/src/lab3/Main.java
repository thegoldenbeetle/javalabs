package lab3;

import java.io.*;
import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        try {
            String inputString = stringInput("input.txt");
            System.out.println(inputString);
            String[] stringArray = toFragments(inputString, 3);
            charToRandomExceptNeighboursByIndex(stringArray, 1);
            Arrays.sort(stringArray);
            System.out.println(Arrays.toString(stringArray));
        } catch (IOException e) {
            e.printStackTrace();
        };
    }

    public static String stringInput(String filename) throws IOException {
        FileReader fileReader = new FileReader(new File(filename));
        BufferedReader reader = new BufferedReader(fileReader);
        return reader.readLine();
    }

    public static String[] toFragments(String string, int numOfFragments) {
        int arrayLength = string.length() / numOfFragments;
        String[] resultArray = new String[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            resultArray[i] = string.substring(i * numOfFragments, (i + 1) * numOfFragments);
        }
        return resultArray;
    }

    public static void charToRandomExceptNeighboursByIndex(String[] stringArray, int charIndex) {
        for (int i = 0; i < stringArray.length; i++) {
            String currentString = stringArray[i];
            char randomChar = getRandomCharExcept(currentString.charAt(charIndex - 1), currentString.charAt(charIndex), currentString.charAt(charIndex + 1));
            stringArray[i] = currentString.substring(0, charIndex) + randomChar + currentString.substring(charIndex + 1);
        }
    }

    public static char getRandomCharExcept(char a, char b, char c) {
        Random random = new Random();
        int randomInt = 0;
        do {
            randomInt = random.nextInt(26 );
        } while (randomInt == (a - '0') || randomInt == (b - '0') || randomInt == (c - '0'));
        return (char) (randomInt + 'a');
    }
}
