package lab5;

import java.util.Scanner;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        System.out.println("The program will quit when you press enter");
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter number: ");
            String string = scanner.nextLine();
            if (string.equals("")) {
                break;
            }
            try {
                int inputNumber = Integer.parseInt(string);
                Stack<Character> stack = numberToStackByDigits(inputNumber);
                poppingItemsFromStack(stack);
            } catch (NumberFormatException ex) {
                System.out.println("It was not a number!");
            }
        }
    }

    public static Stack<Character> numberToStackByDigits(int number) {
        Stack<Character> stack = new Stack<>();
        char[] digits = String.valueOf(number).toCharArray();
        for (char digit : digits) {
            stack.push(digit);
        }
        return stack;
    }

    public static void poppingItemsFromStack(Stack<Character> stack) {
        while (!stack.empty())
        {
            char nextDigit = stack.pop();
            System.out.print(nextDigit);
        }
        System.out.println();
    }
}
