package course_work;

import java.util.ArrayList;
import java.util.Date;

public class Timetable {

    private ArrayList<Record> records;

    public Timetable(String pathToTimetable) {
        records = readTimeTableFromCsv(pathToTimetable);
    }

    ArrayList<Record> readTimeTableFromCsv(String pathToTimetable) {
        return new ArrayList<Record>();
    }
}

class Record {
    private String shipName;
    private Date arrivalDateTime;
    private Cargo cargo;
    private Date parkingTime;
}

enum Cargo {
    LIQUID
}