import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ShipServiceSystemConstructorTests {

    @BeforeClass
    public static void globalSetUp() {
        System.out.println("Initial setup...");
        System.out.println("Code executes only once");
    }

    @Before
    public void setUp() {
        System.out.println("Code executes before each test method");
    }

    @Test
    public void test1() throws Exception {
        ShipServiceSystem shipServiceSystem = new ShipServiceSystem("test_timetable.csv",
                1000, 30000, 1, 30,7, 10);
        Timetable timetable = new Timetable("test_timetable.csv");
        for (int i = 0; i < timetable.getTimeTable().size(); ++i) {
            System.out.print(timetable.getTimeTable().get(i).getShipName());
            System.out.print(' ');
            System.out.print(timetable.getTimeTable().get(i).getArrivalDateTime());
            System.out.print(' ');
            System.out.print(timetable.getTimeTable().get(i).getCargoType());
            System.out.print(' ');
            System.out.print(timetable.getTimeTable().get(i).getCargoWeight());
            System.out.print(' ');
            System.out.print(timetable.getTimeTable().get(i).getParkingDays());
            System.out.println();
        }
    }
}
