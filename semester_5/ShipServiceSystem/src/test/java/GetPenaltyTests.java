import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import static org.junit.Assert.*;


public class GetPenaltyTests {

    @BeforeClass
    public static void globalSetUp() {
        System.out.println("Initial setup...");
        System.out.println("Code executes only once");
    }

    @Before
    public void setUp() {
        System.out.println("Code executes before each test method");
    }

    /*@Test
    public void getPenaltyEmptyRecords() throws Exception {
        ArrayList<TimeRange> records = new ArrayList<>();
        double penalty = ShipServiceSystem.getPenalty(0, records, 1000);
        assertEquals(0, penalty, 0.001);
    }

    @Test
    public void getPenaltyNoOverlaps() throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.ENGLISH);
        ArrayList<TimeRange> records = new ArrayList<>();
        records.add(new TimeRange(formatter.parse("22-08-2020 10:15:55 AM"), formatter.parse("26-08-2020 10:15:55 AM")));
        records.add(new TimeRange(formatter.parse("28-08-2020 09:15:05 AM"), formatter.parse("30-08-2020 12:30:45 AM")));
        for (int i = 1; i < 10; i++) {
            double penalty = ShipServiceSystem.getPenalty(i, records, 1000);
            assertEquals(0, penalty, 0.001);
        }
    }

    @Test
    public void getPenaltyOneOverlapTest1() throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.ENGLISH);
        ArrayList<TimeRange> records = new ArrayList<>();
        records.add(new TimeRange(formatter.parse("10-08-2020 10:15:55 AM"), formatter.parse("20-08-2020 10:15:55 AM")));
        records.add(new TimeRange(formatter.parse("16-08-2020 09:15:05 AM"), formatter.parse("30-08-2020 12:30:45 AM")));
        double penalty = ShipServiceSystem.getPenalty(1, records, 1000);
        assertEquals(4000, penalty, 0.001);
    }

    @Test
    public void getPenaltyTwoCloseTest2() throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.ENGLISH);
        ArrayList<TimeRange> records = new ArrayList<>();
        records.add(new TimeRange(formatter.parse("10-08-2020 10:15:55 AM"), formatter.parse("20-08-2020 10:15:55 AM")));
        records.add(new TimeRange(formatter.parse("20-08-2020 09:15:05 AM"), formatter.parse("30-08-2020 12:30:45 AM")));
        double penalty = ShipServiceSystem.getPenalty(1, records, 1000);
        assertEquals(0, penalty, 0.001);
    }

    @Test
    public void getPenaltyOneOverlapTest3() throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.ENGLISH);
        ArrayList<TimeRange> records = new ArrayList<>();
        records.add(new TimeRange(formatter.parse("10-08-2020 10:15:55 AM"), formatter.parse("20-08-2020 10:15:55 AM")));
        records.add(new TimeRange(formatter.parse("16-08-2020 09:15:05 AM"), formatter.parse("30-08-2020 12:30:45 AM")));
        double penalty = ShipServiceSystem.getPenalty(2, records, 1000);
        assertEquals(0, penalty, 0.001);
    }

    @Test
    public void getPenaltyManyOverlapsTest1() throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.ENGLISH);
        ArrayList<TimeRange> records = new ArrayList<>();
        records.add(new TimeRange(formatter.parse("1-08-2020 10:15:55 AM"), formatter.parse("8-08-2020 10:15:55 AM")));
        records.add(new TimeRange(formatter.parse("3-08-2020 09:15:05 AM"), formatter.parse("27-08-2020 12:30:45 AM")));
        records.add(new TimeRange(formatter.parse("16-08-2020 10:15:55 AM"), formatter.parse("30-08-2020 10:15:55 AM")));
        records.add(new TimeRange(formatter.parse("16-08-2020 09:15:05 AM"), formatter.parse("25-08-2020 12:30:45 AM")));
        double penalty = ShipServiceSystem.getPenalty(1, records, 1000);
        assertEquals(33000, penalty, 0.001);
    }

    @Test
    public void getPenaltyManyOverlapsTest2() throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.ENGLISH);
        ArrayList<TimeRange> records = new ArrayList<>();
        records.add(new TimeRange(formatter.parse("1-08-2020 10:15:55 AM"), formatter.parse("8-08-2020 10:15:55 AM")));
        records.add(new TimeRange(formatter.parse("3-08-2020 09:15:05 AM"), formatter.parse("27-08-2020 12:30:45 AM")));
        records.add(new TimeRange(formatter.parse("16-08-2020 10:15:55 AM"), formatter.parse("30-08-2020 10:15:55 AM")));
        records.add(new TimeRange(formatter.parse("16-08-2020 09:15:05 AM"), formatter.parse("25-08-2020 12:30:45 AM")));
        double penalty = ShipServiceSystem.getPenalty(2, records, 1000);
        assertEquals(8000, penalty, 0.001);
    }

    @Test
    public void getPenaltyManyOverlapsTest3() throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.ENGLISH);
        ArrayList<TimeRange> records = new ArrayList<>();
        records.add(new TimeRange(formatter.parse("03-10-2020 10:15:55 AM"), formatter.parse("08-10-2020 10:15:55 AM")));
        records.add(new TimeRange(formatter.parse("05-10-2020 09:15:05 AM"), formatter.parse("15-10-2020 12:30:45 AM")));
        double penalty = ShipServiceSystem.getPenalty(1, records, 1000);
        assertEquals(4000, penalty, 0.001);
    }*/

}
