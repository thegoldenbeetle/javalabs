import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import static org.junit.Assert.*;


public class MaxOverlapsCountTests {

    private SimpleDateFormat _formatter;

    public MaxOverlapsCountTests() {
        _formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");
    }

    @BeforeClass
    public static void globalSetUp() {
        System.out.println("Initial setup...");
        System.out.println("Code executes only once");
    }

    @Before
    public void setUp() {
        System.out.println("Code executes before each test method");
    }

   /* @Test
    public void maxOverlapsEmpty() throws Exception {
        ArrayList<ShipInfo> records = new ArrayList<>();
        int maxOvelraps = ShipServiceSystem.getMaxOverlaps(records);
        assertEquals(0, maxOvelraps);
    }

    @Test
    public void maxOverlapsTest2() throws Exception {
        ArrayList<ShipInfo> records = new ArrayList<>();
        records.add(new ShipInfo("ship1", _formatter.parse("22.08.2020 10:15"), CargoType.LIQUID, 200, 4, _formatter.parse("26.08.2020 10:15")));
        records.add(new ShipInfo("ship2", _formatter.parse("23.08.2020 09:15"), CargoType.LIQUID, 300, 7, _formatter.parse("30.08.2020 12:30")));
        int maxOvelraps = ShipServiceSystem.getMaxOverlaps(records);
        assertEquals(2, maxOvelraps);
    }

    @Test
    public void maxOverlapsTest3() throws Exception {
        ArrayList<ShipInfo> records = new ArrayList<>();
        records.add(new ShipInfo("ship1", _formatter.parse("22.08.2020 10:15"), CargoType.LIQUID, 200, 4, _formatter.parse("26.08.2020 10:15")));
        records.add(new ShipInfo("ship2", _formatter.parse("28.08.2020 09:15"), CargoType.LIQUID, 300, 2, _formatter.parse("30.08.2020 12:30")));
        int maxOvelraps = ShipServiceSystem.getMaxOverlaps(records);
        assertEquals(1, maxOvelraps);
    }

    @Test
    public void maxOverlapsTest4() throws Exception {
        ArrayList<ShipInfo> records = new ArrayList<>();
        records.add(new ShipInfo("ship1", _formatter.parse("22.08.2020 10:15"), CargoType.LIQUID, 200, 4, _formatter.parse("26.08.2020 10:15")));
        records.add(new ShipInfo("ship2", _formatter.parse("26.08.2020 10:00"), CargoType.LIQUID, 300, 4, _formatter.parse("30.08.2020 12:30")));
        int maxOvelraps = ShipServiceSystem.getMaxOverlaps(records);
        assertEquals(1, maxOvelraps);
    }

    @Test
    public void maxOverlapsTest5() throws Exception {
        ArrayList<ShipInfo> records = new ArrayList<>();
        records.add(new ShipInfo("ship1", _formatter.parse("10.08.2020 10:15"), CargoType.LIQUID, 200, 5, _formatter.parse("15.08.2020 10:15")));
        records.add(new ShipInfo("ship2", _formatter.parse("20.08.2020 10:00"), CargoType.LIQUID, 300, 10, _formatter.parse("30.08.2020 12:30")));
        records.add(new ShipInfo("ship3", _formatter.parse("13.08.2020 10:00"), CargoType.LIQUID, 300, 13, _formatter.parse("26.08.2020 12:30")));
        int maxOvelraps = ShipServiceSystem.getMaxOverlaps(records);
        assertEquals(2, maxOvelraps);
    }

    @Test
    public void maxOverlapsTest6() throws Exception {
        ArrayList<ShipInfo> records = new ArrayList<>();
        records.add(new ShipInfo("ship2", _formatter.parse("20.08.2020 10:00"), CargoType.LIQUID, 300, 10, _formatter.parse("30.08.2020 12:30")));
        records.add(new ShipInfo("ship3", _formatter.parse("13.08.2020 10:00"), CargoType.LIQUID, 300, 13, _formatter.parse("26.08.2020 12:30")));
        records.add(new ShipInfo("ship1", _formatter.parse("10.08.2020 10:15"), CargoType.LIQUID, 200, 5, _formatter.parse("15.08.2020 10:15")));
        records.add(new ShipInfo("ship4", _formatter.parse("14.08.2020 10:00"), CargoType.LIQUID, 300, 13, _formatter.parse("27.08.2020 12:30")));
        int maxOvelraps = ShipServiceSystem.getMaxOverlaps(records);
        assertEquals(3, maxOvelraps);
    }

    @Test
    public void maxOverlapsTest7() throws Exception {
        ArrayList<ShipInfo> records = new ArrayList<>();
        records.add(new ShipInfo("ship2", _formatter.parse("10.08.2020 10:00"), CargoType.LIQUID, 300, 5, _formatter.parse("15.08.2020 12:30")));
        records.add(new ShipInfo("ship3", _formatter.parse("11.08.2020 10:00"), CargoType.LIQUID, 300, 3, _formatter.parse("14.08.2020 12:30")));
        records.add(new ShipInfo("ship1", _formatter.parse("12.08.2020 10:15"), CargoType.LIQUID, 200, 1, _formatter.parse("13.08.2020 10:15")));
        int maxOvelraps = ShipServiceSystem.getMaxOverlaps(records);
        assertEquals(3, maxOvelraps);
    }*/
}
