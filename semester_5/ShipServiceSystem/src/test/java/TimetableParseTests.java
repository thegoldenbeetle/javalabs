import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;

public class TimetableParseTests {

    @BeforeClass
    public static void globalSetUp() {
        System.out.println("Initial setup...");
        System.out.println("Code executes only once");
    }

    @Before
    public void setUp() {
        System.out.println("Code executes before each test method");
    }

    @Test
    public void parseEmptyTimeTable() throws Exception {
    }

    @Test
    public void parseTimeTable5Lines() throws Exception {
        Timetable timetable = new Timetable("test_timetable.csv");
        for (int i = 0; i < timetable.getTimeTable().size(); ++i) {
            System.out.print(timetable.getTimeTable().get(i).getShipName());
            System.out.print(' ');
            System.out.print(timetable.getTimeTable().get(i).getArrivalDateTime());
            System.out.print(' ');
            System.out.print(timetable.getTimeTable().get(i).getCargoType());
            System.out.print(' ');
            System.out.print(timetable.getTimeTable().get(i).getCargoWeight());
            System.out.print(' ');
            System.out.print(timetable.getTimeTable().get(i).getParkingDays());
            System.out.println();
        }
    }

}
