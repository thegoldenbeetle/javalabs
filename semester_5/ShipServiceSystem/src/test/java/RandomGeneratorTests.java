import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;

public class RandomGeneratorTests {

    private RandomGenerator _randomGenerator = new RandomGenerator();

    @BeforeClass
    public static void globalSetUp() {
        System.out.println("Initial setup...");
        System.out.println("Code executes only once");
    }

    @Before
    public void setUp() {
        System.out.println("Code executes before each test method");
    }

    @Test
    public void generateRandoms() throws Exception {
        ArrayList<Integer> statisticRandom = new ArrayList<>(15);
        for (int i = 0; i < 15; ++i) {
            statisticRandom.add(0);
        }
        for (int i = 0; i < 10000; ++i) {
            int randomNumber = _randomGenerator.generateGaussian(0, 7.0 / 3.0, -7, 7);
            statisticRandom.set(randomNumber + 7, statisticRandom.get(randomNumber + 7) + 1);
        }
        System.out.println(statisticRandom.toString());
    }

    @Test
    public void generateRandomOneTime() throws Exception {
        for (int i = 0; i < 10; i++) {
            int randomNumber = _randomGenerator.generateGaussian(0, 10 / 3.0, -10, 10);
            System.out.println(randomNumber);
        }
    }

}
