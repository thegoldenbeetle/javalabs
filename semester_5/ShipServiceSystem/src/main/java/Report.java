import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;

public class Report {

    HashMap<CargoType, DataForReport> _shipsData;

    int _totalUnloadingShips;
    double _totalPenalty;
    double _meanDaysInQueue;
    double _meanQueueLength;
    double _maxUnloadingDelay;
    double _meanUnloadingDelay;

    public Report(HashMap<CargoType, DataForReport> shipsData, int period) {
        _shipsData = shipsData;
        _totalPenalty = 0;
        _totalUnloadingShips = 0;
        _meanDaysInQueue = 0;
        _maxUnloadingDelay = 0;
        _meanUnloadingDelay = 0;
        _meanQueueLength = 0;
        int counterUnloadingDelay = 0;
        for (CargoType cargoType : CargoType.values()) {
            DataForReport data = _shipsData.get(cargoType);
            for (String shipName : data._shipsData.keySet()) {
                if (data._shipsData.get(shipName)._isUnload == 1) {
                    Date arrivalDate = data._shipsData.get(shipName)._arrivalDate;
                    Date beginUnloadingDate = data._shipsData.get(shipName)._beginUnloadingDate;
                    data._shipsData.get(shipName)._daysInQueue = DateTimeFunctions.twoDatesDifferenceInDays(beginUnloadingDate, arrivalDate);
                    _meanDaysInQueue += data._shipsData.get(shipName)._daysInQueue;
                    _totalUnloadingShips += 1;
                    if (data._shipsData.get(shipName)._daysDelayUnloading > _maxUnloadingDelay) {
                        _maxUnloadingDelay = data._shipsData.get(shipName)._daysDelayUnloading;
                    }
                    _meanUnloadingDelay += data._shipsData.get(shipName)._daysDelayUnloading;
                    counterUnloadingDelay += 1;
                }
            }
            _totalPenalty += data._penalty;
            _meanQueueLength += data._sumQueueLength;
        }
        _meanDaysInQueue /= _totalUnloadingShips;
        _meanUnloadingDelay /= counterUnloadingDelay;
        _meanQueueLength /= period;
    }

    public void showShipUnloadingReport() {
        System.out.println("Ship unloading report");
        System.out.println("Ship name; Arrival date; Unloading date; Days in queue; Days in unloading; Is unload");
        for (CargoType cargoType : CargoType.values()) {
            DataForReport data = _shipsData.get(cargoType);
            for (String shipName : data._shipsData.keySet()) {
                System.out.print(data._shipsData.get(shipName));
                System.out.println();
            }
        }
    }

    public void showTotalStatisticReport() {
        System.out.println("Total statistic report");
        System.out.println("Total unloading ships: " + _totalUnloadingShips);
        System.out.println("Mean days in queue: " + _meanDaysInQueue);
        System.out.println("Mean queue length: " + _meanQueueLength);
        System.out.println("Max unloading delay: " + _maxUnloadingDelay);
        System.out.println("Mean unloading delay: " + _meanUnloadingDelay);
        System.out.println("Total penalty: " + _totalPenalty);
        System.out.println("Penalty by cargo types: ");
        for (CargoType cargoType : CargoType.values()) {
            DataForReport data = _shipsData.get(cargoType);
            System.out.println(cargoType + " " + data._penalty);
        }
        System.out.println("Cranes by cargo types: ");
        for (CargoType cargoType : CargoType.values()) {
            DataForReport data = _shipsData.get(cargoType);
            System.out.println(cargoType + " " + data._cranes);
        }
    }

    public void save(String shipUnloadingReportPath, String totalStatisticReportPath) {
        saveShipUnloadingReport(shipUnloadingReportPath);
        saveTotalStatisticReport(totalStatisticReportPath);
    }

    public void saveShipUnloadingReport(String shipUnloadingReportPath) {
        try {
            CSVWriter writer = new CSVWriter(new FileWriter(shipUnloadingReportPath));
            String [] header = "Ship name; Arrival date; Unloading date; Days in queue; Days in unloading;".split(";");
            writer.writeNext(header);
            for (CargoType cargoType : CargoType.values()) {
                DataForReport data = _shipsData.get(cargoType);
                for (String shipName : data._shipsData.keySet()) {
                    if (data._shipsData.get(shipName)._isUnload == 1) {
                        var shipsData = data._shipsData.get(shipName);
                        String [] line = new String[] {shipsData._shipName, shipsData._arrivalDate.toString(),
                                                       shipsData._beginUnloadingDate.toString(), String.valueOf(shipsData._daysInQueue),
                                                       String.valueOf(shipsData._daysUnloading)};
                        writer.writeNext(line);
                    }
                }
            }
            writer.close();
        } catch (IOException e) {
            System.out.println("Ship unloading report was not saved");
        }
    }

    public void saveTotalStatisticReport(String totalStatisticReportPath)  {
        try {
            PrintWriter writer = new PrintWriter(totalStatisticReportPath, StandardCharsets.UTF_8);
            writer.println("Total unloading ships: " + _totalUnloadingShips);
            writer.println("Mean days in queue: " + _meanDaysInQueue);
            writer.println("Mean queue length: " + _meanQueueLength);
            writer.println("Max unloading delay: " + _maxUnloadingDelay);
            writer.println("Mean unloading delay: " + _meanUnloadingDelay);
            writer.println("Total penalty: " + _totalPenalty);
            writer.println("Penalty by cargo types: ");
            for (CargoType cargoType : CargoType.values()) {
                DataForReport data = _shipsData.get(cargoType);
                writer.println(cargoType + " " + data._penalty);
            }
            writer.println("Cranes by cargo types: ");
            for (CargoType cargoType : CargoType.values()) {
                DataForReport data = _shipsData.get(cargoType);
                writer.println(cargoType + " " + data._cranes);
            }
            writer.close();
        } catch (IOException e) {
            System.out.println("Total statistic report was not saved");
        }
    }
}

class ReportItem {
    public String _shipName;
    public Date _arrivalDate;
    public int _daysInQueue;
    public Date _beginUnloadingDate;
    public int _daysUnloading;
    public int _daysDelayUnloading;
    public int _isUnload;

    public String toString() {
        String beginUnloading = (_beginUnloadingDate == null) ? "not begin unloading" : _beginUnloadingDate.toString();
        return _shipName + "; " + _arrivalDate.toString() + "; " + beginUnloading +
                "; " + _daysInQueue + "; " + _daysUnloading + "; " + _isUnload;
    }
}

// for one type of cargo
class DataForReport {

    public DataForReport(HashMap<String, ReportItem> shipsData, double penalty, int cranes, int sumQueueLength) {
        _penalty = penalty;
        _shipsData = shipsData;
        _cranes = cranes;
        _sumQueueLength = sumQueueLength;
    }

    public double _penalty;
    public int _cranes;
    public int _sumQueueLength;
    public HashMap<String, ReportItem> _shipsData;
}