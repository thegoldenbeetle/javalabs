import com.opencsv.CSVReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

enum CargoType {
    BULK,
    LIQUID,
    CONTAINER
}

public class Timetable {

    private final String DATE_TIME_FORMAT_STRING = "dd.MM.yyyy HH:mm";
    private enum ColumnsName {
        SHIP_NAME,
        ARRIVAL_DATE_TIME,
        CARGO_TYPE,
        CARGO_WEIGHT,
        PARKING_TIME
    }
    private final HashMap<String, CargoType> cargoTypesMap = new HashMap<>() {{
        put("LIQUID", CargoType.LIQUID);
        put("BULK", CargoType.BULK);
        put("CONTAINER", CargoType.CONTAINER);
    }};

    private ArrayList<Record> _records;

    public Timetable(String pathToTimetable) throws IOException, ParseException {
        _records = readTimeTableFromCsv(pathToTimetable);
    }

    public ArrayList<Record> readTimeTableFromCsv(String pathToTimetable) throws IOException, ParseException {
        ArrayList<Record> result = new ArrayList<>();
        CSVReader reader = new CSVReader(new FileReader(pathToTimetable), ';' , '"' , 1);
        String[] nextLine;
        DateFormat format = new SimpleDateFormat(DATE_TIME_FORMAT_STRING);
        while ((nextLine = reader.readNext()) != null) {
            String shipName = nextLine[ColumnsName.SHIP_NAME.ordinal()];
            Date arrivalDate = format.parse(nextLine[ColumnsName.ARRIVAL_DATE_TIME.ordinal()]);
            CargoType cargoType = cargoTypesMap.get(nextLine[ColumnsName.CARGO_TYPE.ordinal()]);
            if (cargoType == null) {
                throw new ParseException("Error in parsing cargo type", 0);
            }
            double cargoWeight = Double.parseDouble(nextLine[ColumnsName.CARGO_WEIGHT.ordinal()]);
            int parkingDays = Integer.parseInt(nextLine[ColumnsName.PARKING_TIME.ordinal()]);
            result.add(new Record(shipName, arrivalDate, cargoType, cargoWeight, parkingDays));
        }
        return result;
    }

    public ArrayList<Record> getTimeTable() {
        return _records;
    }
}

class Record {

    public Record(String shipName, Date arrivalDateTime, CargoType cargoType, double cargoWeight, int parkingDays) {
        _shipName = shipName;
        _arrivalDateTime = arrivalDateTime;
        _cargoWeight = cargoWeight;
        _cargoType = cargoType;
        _parkingDays = parkingDays;
    }

    private final String _shipName;
    private final Date _arrivalDateTime;
    private final double _cargoWeight;
    private final CargoType _cargoType;
    private final int _parkingDays;

    public String getShipName() {
        return _shipName;
    }

    public Date getArrivalDateTime() {
        return _arrivalDateTime;
    }

    public double getCargoWeight() {
        return _cargoWeight;
    }

    public CargoType getCargoType() {
        return _cargoType;
    }

    public int getParkingDays() {
        return _parkingDays;
    }
}
