import java.util.Calendar;
import java.util.Date;

public class DateTimeFunctions {

    public static Date addDays(Date date, int numOfDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, numOfDays);
        return calendar.getTime();
    }

    public static int twoDatesDifferenceInDays(Date date1, Date date2) {
        return (int) ((date1.getTime() - date2.getTime()) / (24 * 60 * 60 * 1000));
    }
}
