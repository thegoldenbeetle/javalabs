import java.io.IOException;
import java.text.ParseException;


public class Main {

    public static void main(String[] args) {

        double penaltyPricePerDay = 1000;
        double cranPrice = 0;
        int simulateStep = 1;
        int period = 30;
        int maxDelayDurationDays = 10;
        int arrivalDaysError = 7;

        try {
            ShipServiceSystem shipServiceSystem = new ShipServiceSystem("timetable_big.csv", penaltyPricePerDay,
                                                cranPrice, simulateStep, period, maxDelayDurationDays, arrivalDaysError);
            shipServiceSystem.simulate();
            Report report = shipServiceSystem.getReport();
            report.showTotalStatisticReport();
            report.showShipUnloadingReport();
            report.save("ship_unloading_report.csv", "total_statistic.txt");
        } catch (IOException e) {
            System.out.println("File with timetable was not found");
        } catch (ParseException e) {
            System.out.println("Error in parsing file with timetable.");
            System.out.println("Check format is correct.");
        }
    }


}
