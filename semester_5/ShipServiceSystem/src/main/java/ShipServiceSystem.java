import org.joda.time.DateTimeComparator;

import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Integer.max;
import static java.lang.Integer.min;


public class ShipServiceSystem {

    HashMap<CargoType, ArrayList<ShipInfo>> _shipsMap;
    HashMap<CargoType, DataForReport> _shipsDataForReport;

    double _penaltyPricePerDay;
    double _cranPrice;
    int _simulateStep;
    int _maxDelayDurationDays;
    int _arrivalDaysError;
    int _period;

    Date _start;
    Date _end;

    public ShipServiceSystem(String pathToTimetable, double penaltyPricePerDay, double cranPrice,
                             int simulateStep, int period, int maxDelayDurationDays, int arrivalDaysError) throws IOException, ParseException {

        _maxDelayDurationDays = maxDelayDurationDays;
        _arrivalDaysError = arrivalDaysError;

        Timetable timetable = new Timetable(pathToTimetable);
        ArrayList<ShipInfo> shipsInfo = addAdditionalInfo(addRandomToShipsInfo(timetableToShipsInfo(timetable)));

        Optional<ShipInfo> min = shipsInfo.stream().min(Comparator.comparing(obj -> obj._arrivalDateTime));
        _start = min.get()._arrivalDateTime;
        _end = DateTimeFunctions.addDays(_start, period);
        _period = period;

        _shipsMap = new HashMap<>();
        Arrays.stream(CargoType.values()).forEach(cargoType -> _shipsMap.put(cargoType, new ArrayList<>()));
        shipsInfo.forEach(shipInfo -> _shipsMap.get(shipInfo._cargoType).add(shipInfo));

        _penaltyPricePerDay = penaltyPricePerDay;
        _cranPrice = cranPrice;
        _simulateStep = simulateStep;
    }

    private ArrayList<ShipInfo> timetableToShipsInfo(Timetable timetable) {
        ArrayList<Record> records = timetable.getTimeTable();
        return records.stream().map(timeTableItem ->
                new ShipInfo(timeTableItem.getShipName(), timeTableItem.getArrivalDateTime(), timeTableItem.getCargoType(),
                        timeTableItem.getCargoWeight(), timeTableItem.getParkingDays()
                )
        ).collect(Collectors.toCollection(ArrayList::new));
    }

    private ArrayList<ShipInfo> addRandomToShipsInfo(ArrayList<ShipInfo> timetable) {
        ArrayList<ShipInfo> result = new ArrayList<>();

        RandomGenerator randomGeneratorArrival = new RandomGenerator();
        RandomGenerator randomGeneratorDuration = new RandomGenerator();

        for (ShipInfo shipInfo: timetable) {
            int delayParkingDays = Math.abs(randomGeneratorDuration.generateGaussian(0, _maxDelayDurationDays / 3.0, -_maxDelayDurationDays, _maxDelayDurationDays));
            int delayArrivalDays = randomGeneratorArrival.generateGaussian(0, _arrivalDaysError / 3.0, -_arrivalDaysError, _arrivalDaysError);
            ShipInfo newShipInfo = new ShipInfo(shipInfo._shipName,
                    DateTimeFunctions.addDays(shipInfo._arrivalDateTime, delayArrivalDays),
                    shipInfo._cargoType, shipInfo._cargoWeight,
                    shipInfo._parkingDays + delayParkingDays,
                    delayParkingDays);
            result.add(newShipInfo);
        }

        return result;
    }

    private ArrayList<ShipInfo> addAdditionalInfo(ArrayList<ShipInfo> timetable) {
        // add departure date time
        return timetable.stream().map(timeTableItem ->
                new ShipInfo(timeTableItem._shipName, timeTableItem._arrivalDateTime, timeTableItem._cargoType,
                             timeTableItem._cargoWeight, timeTableItem._parkingDays, timeTableItem._daysDelayUnloading,
                             DateTimeFunctions.addDays(timeTableItem._arrivalDateTime, timeTableItem._parkingDays)
                )
        ).collect(Collectors.toCollection(ArrayList::new));
    }

    public void simulate() {
        _shipsDataForReport = new HashMap<>();
        Arrays.stream(CargoType.values()).forEach(cargoType -> _shipsDataForReport.put(cargoType, new DataForReport(new HashMap<>(), 0, 0, 0)));
        for (CargoType cargoType : CargoType.values()) {
            simulateOneType(cargoType);
        }
    }

    public void simulateOneType(CargoType cargoType) {
        System.out.println("Simulate: " + cargoType.toString());
        if (_shipsMap.get(cargoType).size() == 0) {
            return;
        }
        int overlapsNum = getMaxOverlaps(_shipsMap.get(cargoType));
        System.out.println("Max needed cranes:" + overlapsNum);
        for (int i = 1; i <= overlapsNum; i++) {
            DataForReport result = getPenalty(i, _shipsMap.get(cargoType), _penaltyPricePerDay);
            double penalty = result._penalty;
            System.out.print("cranes: " + i);
            System.out.print(' ');
            System.out.print("penalty: " + penalty);
            System.out.println();
            if (penalty <= _cranPrice) {
                System.out.println("final cranes: " + i);
                System.out.println("final penalty: " + penalty);
                _shipsDataForReport.put(cargoType, result);
                break;
            }
        }
    }

    // for one type of cargo
    public int getMaxOverlaps(ArrayList<ShipInfo> records) {

        // 1 - end
        // 0 - start
        class TimeRangeWithFlag {
            TimeRangeWithFlag(Date date, int flag) {
                _date = date;
                _flag = flag;
            }
            int getFlag() {
                return _flag;
            }
            private final Date _date;
            private final int _flag;
        }

        ArrayList<TimeRangeWithFlag> timeRanges = new ArrayList<>();
        for (ShipInfo record : records) {
            if (record._arrivalDateTime.after(_start) && record._arrivalDateTime.before(_end)
                || record._arrivalDateTime.equals(_start) || record._arrivalDateTime.equals(_end)) {
                timeRanges.add(new TimeRangeWithFlag(record._arrivalDateTime, 0));
                timeRanges.add(new TimeRangeWithFlag(DateTimeFunctions.addDays(record._departureDateTime, _simulateStep), 1));
            }
        }

        DateTimeComparator dateOnlyInstance = DateTimeComparator.getDateOnlyInstance();
        timeRanges.sort((object1, object2) -> dateOnlyInstance.compare(object1._date, object2._date));
        int maxCount = 0;
        int count = 0;
        for (TimeRangeWithFlag timeRange : timeRanges) {
            count = (timeRange.getFlag() == 0) ? count + 1 : count - 1;
            maxCount = max(maxCount, count);
        }
        return maxCount;
    }

    // for one type of cargo
    public DataForReport getPenalty(int cranesNumber, ArrayList<ShipInfo> ships, double penaltySumOneDay) {

        ArrayList<ShipInfo> records = new ArrayList<>(ships);
        records.sort(Comparator.comparing(obj -> obj._arrivalDateTime));

        int sumQueueLength = 0;
        ShipsQueue shipsQueue = new ShipsQueue(cranesNumber, _simulateStep);

        Calendar iterDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();
        endDate.setTime(_end);
        iterDate.setTime(_start);
        while(iterDate.before(endDate)) {

            shipsQueue.unloadShips();

            sumQueueLength += (shipsQueue.getCurrentWaitingQueueLength() * _simulateStep);
            shipsQueue.waitingShipsToUnloadingQueue(iterDate.getTime());

            shipsQueue.addNewArrivedShips(iterDate.getTime(), records);

            iterDate.add(Calendar.DAY_OF_MONTH, _simulateStep);
        }

        return new DataForReport(shipsQueue.getReport(), shipsQueue.getDaysPenalty() * penaltySumOneDay,
                                 cranesNumber, sumQueueLength);
    }

    public Report getReport() {
        return new Report(_shipsDataForReport, _period);
    }

}

class ShipInfo {
    public String _shipName;
    public Date _arrivalDateTime;
    public double _cargoWeight;
    public CargoType _cargoType;
    public int _parkingDays;
    public Date _departureDateTime;
    public int _daysDelayUnloading;

    public ShipInfo(String shipName, Date arrivalDateTime, CargoType cargoType, double cargoWeight, int parkingDays) {
        _shipName = shipName;
        _arrivalDateTime = arrivalDateTime;
        _cargoType = cargoType;
        _cargoWeight = cargoWeight;
        _parkingDays = parkingDays;
    }

    public ShipInfo(String shipName, Date arrivalDateTime, CargoType cargoType, double cargoWeight, int parkingDays, int daysDelayUnloading) {
        this(shipName, arrivalDateTime, cargoType, cargoWeight, parkingDays);
        _daysDelayUnloading = daysDelayUnloading;
    }

    public ShipInfo(String shipName, Date arrivalDateTime, CargoType cargoType, double cargoWeight, int parkingDays, int daysDelayUnloading, Date departureDateTime) {
        this(shipName, arrivalDateTime, cargoType, cargoWeight, parkingDays, daysDelayUnloading);
        _departureDateTime = departureDateTime;
    }
}
