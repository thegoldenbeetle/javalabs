import org.joda.time.DateTimeComparator;

import java.util.*;

import static java.lang.Integer.min;

public class ShipsQueue {

    private final ArrayList<ShipInfo> _newShips;
    private final LinkedList<ShipItem> _unloadingShips;
    private final Queue<ShipItem> _waitingShips;

    private final HashMap<String, ReportItem> _report;

    private int _freeCranes;
    private final int _simulateStep;
    private int _daysPenalty;

    public ShipsQueue(int cranesNumber, int simulateStep) {
        _unloadingShips = new LinkedList<>();
        _waitingShips = new LinkedList<>();
        _newShips = new ArrayList<>();
        _daysPenalty = 0;

        _report = new HashMap<>();

        _freeCranes = cranesNumber;
        _simulateStep = simulateStep;
    }

    public void unloadShips() {
        for (int t = 0; t < _unloadingShips.size(); t++) {
            if (_unloadingShips.get(t).getValue() > 0)
                _unloadingShips.set(t, new ShipItem(_unloadingShips.get(t).getShipName(), _unloadingShips.get(t).getValue() - _simulateStep));
        }
        Iterator<ShipItem> iter = _unloadingShips.iterator();
        while(iter.hasNext()) {
            ShipItem s = iter.next();
            if (s.getValue() <= 0) {
                _report.get(s.getShipName())._isUnload = 1;
                iter.remove();
                _freeCranes += 1;
            }
        }
    }

    public void waitingShipsToUnloadingQueue(Date currentDate) {
        int temp = _waitingShips.size();
        _daysPenalty += temp * _simulateStep;
        if ((_freeCranes > 0) && (_waitingShips.size() > 0)) {
            for (int p = 0; p < min(_freeCranes, temp); ++p) {
                _unloadingShips.add(_waitingShips.element());
                _report.get(_waitingShips.element().getShipName())._beginUnloadingDate = currentDate;
                _waitingShips.remove();
                _freeCranes -= 1;
            }
        }
    }

    public void addNewArrivedShips(Date currentDate, ArrayList<ShipInfo> records) {
        _newShips.clear();
        for (ShipInfo record : records) {
            if (DateTimeComparator.getDateOnlyInstance().compare(record._arrivalDateTime, currentDate) == 0) {
                _newShips.add(record);
            }
            if (_simulateStep == 2 || _simulateStep == 3) {
                Date lastDay = DateTimeFunctions.addDays(currentDate, -1);
                if (DateTimeComparator.getDateOnlyInstance().compare(record._arrivalDateTime, lastDay) == 0) {
                    _daysPenalty += 1; // 1 day it was staying
                    _newShips.add(record);
                }
            }
            if (_simulateStep == 3) {
                Date preLastDay = DateTimeFunctions.addDays(currentDate, -2);
                if (DateTimeComparator.getDateOnlyInstance().compare(record._arrivalDateTime, preLastDay) == 0) {
                    _daysPenalty += 2; // 2 day it was staying
                    _newShips.add(record);
                }
            }
        }

        distributeNewShips(currentDate);
    }

    private void distributeNewShips(Date currentDate) {
        if (_newShips.size() > 0) {
            _newShips.sort((lhs, rhs) -> {
                int compareArrival = DateTimeComparator.getDateOnlyInstance().compare(lhs._arrivalDateTime, rhs._arrivalDateTime);
                if (compareArrival == 0) {
                    return Integer.compare(lhs._parkingDays, rhs._parkingDays);
                }
                return compareArrival;
            });
            for (ShipInfo newShip : _newShips) {
                ReportItem reportItem = new ReportItem();
                reportItem._shipName = newShip._shipName;
                reportItem._arrivalDate = newShip._arrivalDateTime;
                reportItem._daysUnloading = newShip._parkingDays;
                reportItem._daysDelayUnloading = newShip._daysDelayUnloading;
                _report.put(newShip._shipName, reportItem);
                if (_freeCranes > 0) {
                    _unloadingShips.add(new ShipItem(newShip._shipName, newShip._parkingDays));
                    _report.get(newShip._shipName)._beginUnloadingDate = currentDate;
                    _freeCranes -= 1;
                } else {
                    _waitingShips.add(new ShipItem(newShip._shipName, newShip._parkingDays));
                }
            }
        }
    }

    public int getCurrentWaitingQueueLength() {
        return _waitingShips.size();
    }

    public int getDaysPenalty() {
        return _daysPenalty;
    }

    public HashMap<String, ReportItem> getReport() {
        return _report;
    }
}

class ShipItem {

    public ShipItem(String shipName, int value) {
        _shipName = shipName;
        _value = value;
    }

    public String getShipName() {
        return _shipName;
    }

    public int getValue() {
        return _value;
    }

    private final String _shipName;
    private final int _value;
}