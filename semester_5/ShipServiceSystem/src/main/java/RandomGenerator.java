import java.util.Random;

public class RandomGenerator {

    Random _random;

    public RandomGenerator() {
        _random = new Random(123);
    }

    public int generateGaussian(double mean, double std, int leftBorder, int rightBorder) {
        int randomNumber = (int) (_random.nextGaussian() * std + mean);
        randomNumber = (randomNumber > rightBorder) ? rightBorder : (Math.max(randomNumber, leftBorder));
        return randomNumber;
    }
}
