package lab1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try {
            int[] inputArray = arrayInput("input.txt");
            System.out.println("Input array:");
            System.out.println(Arrays.toString(inputArray));
            arrayShuffle(inputArray);
            System.out.println("After shuffle:");
            System.out.println(Arrays.toString(inputArray));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static int[] arrayInput(String filename) throws FileNotFoundException {
        Scanner s = new Scanner(new File(filename));
        int[] array = new int[s.nextInt()];
        for (int i = 0; i < array.length; i++) {
            array[i] = s.nextInt();
        }
        return array;
    }

    public static void arrayShuffle(int[] array) {
        Random random = new Random();

        for (int i = 1; i < array.length; i++) {
            int leftRandIndex = random.nextInt(i);

            int temp = array[i];
            array[i] = array[leftRandIndex];
            array[leftRandIndex] = temp;
        }
    }
}
