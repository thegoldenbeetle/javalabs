package lab7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.emptyList;

public class Main {

    public static void main(String[] args) {

        List<Integer> intListForAverage = List.of(1, 2, 3, 4);
        System.out.println(average(intListForAverage));

        List<String> stringList = List.of("one", "two", "three", "For");
        System.out.println(toUppercase(stringList));

        List<Double> doubleListForSquare = List.of(1.0, 2.0, 3.0, 4.0, 3.0, 3.0, 4.0, 4.0, 4.0);
        System.out.println(listOfSquares(doubleListForSquare));

    }

    public static double average(List<Integer> intList) {
        return (intList == null) ? 0 : intList.stream().mapToInt(e -> e).average().getAsDouble();
    }

    public static List<String> toUppercase(List<String> stringList) {
        return (stringList == null) ? Collections.emptyList() :
                stringList.stream().map(String::toUpperCase).collect(Collectors.toList());
    }

    public static List<Double> listOfSquares(List<Double> doubleList) {
        return (doubleList == null) ? Collections.emptyList() :
               doubleList.stream().filter(e -> Collections.frequency(doubleList, e) == 1).map(e -> e * e).collect(Collectors.toList());
    }
}
