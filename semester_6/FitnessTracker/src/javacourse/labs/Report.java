package javacourse.labs;

public class Report {

    private final String trainName;
    private final double duration;
    private final double calories;

    public Report(String train, double trainDuration, double trainCalories) {
        trainName = train;
        duration = trainDuration;
        calories = trainCalories;
    }

    public String toString() {
        return trainName + ", " + duration + ", " + calories;
    }
}
