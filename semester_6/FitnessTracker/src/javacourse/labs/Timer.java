package javacourse.labs;

public class Timer {

    private long elapsedTimeSeconds;
    private volatile boolean isRunning;
    private Thread thread;

    public void startThread() {
        isRunning = true;
        thread = new Thread(() -> {
            try {
                start();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread.start();
    }

    private void start() throws InterruptedException {
        int charsWritten = 0;
        long start = System.currentTimeMillis();
        while (isRunning) {
            long elapsedTime = System.currentTimeMillis() - start;
            elapsedTimeSeconds = elapsedTime / 1000;

            String seconds = Integer.toString((int) (elapsedTimeSeconds % 60));
            String minutes = Integer.toString((int) ((elapsedTimeSeconds % 3600) / 60));
            String hours = Integer.toString((int) (elapsedTimeSeconds / 3600));

            if (seconds.length() < 2) {
                seconds = "0" + seconds;
            }

            if (minutes.length() < 2) {
                minutes = "0" + minutes;
            }

            if (hours.length() < 2) {
                hours = "0" + hours;
            }

            String writeThis = hours + ":" + minutes + ":" + seconds;

            for (int i = 0; i < charsWritten; i++) {
                System.out.print("\b");
            }
            System.out.print(writeThis);
            charsWritten = writeThis.length();

            Thread.sleep(1000);
        }
    }

    public void finish() {
        isRunning = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean isWorking() {
        return isRunning;
    }

    public long getCurrentDuration() {
        return elapsedTimeSeconds;
    }

}
