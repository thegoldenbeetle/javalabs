package javacourse.labs;


import java.io.*;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Tracker {

    private final Map<Long, Report> statistic;
    private final Scanner scanner;
    private double totalCalories;
    private final String statisticFileName = "statistic.txt";

    public Tracker() {
        statistic = readStatisticFromFile();
        scanner = new Scanner(System.in);
    }

    public void startWork() {
        try {
            String s = "";
            while (!"5".equals(s)) {
                System.out.println("Выберите тренировку:");
                System.out.println("1. Для выбора тренировки 'отжимание' введите 1");
                System.out.println("2. Для выбора тренировки 'скакалка' введите 2");
                System.out.println("3. Для выбора тренировки 'приседание' введите 3");
                System.out.println("4. Для просмотра отчета по тренировкам введите 4");
                System.out.println("5. Для выхода из приложения введите 5");
                s = scanner.nextLine();
                switch (s) {
                    case "1" -> trainSubMenu(new PushUpTraining());
                    case "2" -> trainSubMenu(new SkippingRopeTraining());
                    case "3" -> trainSubMenu(new SquatTraining());
                    case "4" -> showReport();
                    case "5" -> saveStatisticToFile();
                    default -> System.out.println("Нет такой команды!");
                }
            }
        } catch (InterruptedException ex) {
            System.out.println("Ошибка таймера!");
        }
    }

    private Map<Long, Report> readStatisticFromFile() {
        Map<Long, Report> readedStatistic = new TreeMap<>();
        int TIME_START_INDEX = 0;
        int TRAIN_NAME_INDEX = 1;
        int TRAIN_DURATION_INDEX= 2;
        int CALORIES_INDEX = 3;
        try (BufferedReader reader = new BufferedReader(new FileReader(statisticFileName)))
        {
            String line = "";
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(", ");
                readedStatistic.put(Long.valueOf(parts[TIME_START_INDEX]),
                        new Report(parts[TRAIN_NAME_INDEX], Double.parseDouble(parts[TRAIN_DURATION_INDEX]),
                                Double.parseDouble(parts[CALORIES_INDEX])));
                totalCalories += Double.parseDouble(parts[CALORIES_INDEX]);
            }
        } catch (IOException e) {
            return new TreeMap<>();
        }
        return readedStatistic;
    }

    private void trainSubMenu(Training training) throws InterruptedException {
        String s = "";
        while (!"3".equals(s)) {
            boolean isTrain = training.isTrain();
            String currentTrainName = training.getTrainName();
            if (!isTrain) {
                System.out.println("Выбрана тренировка '" + currentTrainName + "'");
                System.out.println("1. Для начала тренировки введите 1");
                System.out.println("2. Для завершения тренировки введите Enter");
                System.out.println("3. Для возврата в предыдущее меню введите 3");
            }
            s = scanner.nextLine();
            switch (s) {
                case "1":
                    if (isTrain) {
                        System.out.println("Тренировка уже была начата!");
                    } else {
                        training.start();
                    }
                    break;
                case "":
                    if (isTrain) {
                        training.finish();
                        double currentTrainDuration = training.getTrainDuration();
                        double currentTrainCalories = training.getCalories();
                        statistic.put(System.currentTimeMillis(), new Report(currentTrainName, currentTrainDuration, currentTrainCalories));
                        totalCalories += training.getCalories();
                        System.out.println("Длительность тренировки " + currentTrainDuration);
                        System.out.println("Количество затраченных калорий " + currentTrainCalories);
                    } else {
                        System.out.println("Тренировка еще не начата!");
                    }
                    break;
                case "3":
                    break;
                default:
                    System.out.println("Нет такой команды!");
            }
        }
    }

    private void showReport() {
        String s = "";
        System.out.println("Для возврата в меню введите Enter");
        System.out.println("Название тренировки, затраченное время (секунды), количество калорий (кал)");
        for (Long time : statistic.keySet()) {
            String value = statistic.get(time).toString();
            System.out.println(value);
        }
        System.out.println(statistic.keySet().isEmpty() ? "Пусто!": "Суммарное количество калорий: " + totalCalories);
        do {
            s = scanner.nextLine();
        } while (!"".equals(s));
    }

    private void saveStatisticToFile() {
        try (PrintWriter out = new PrintWriter(statisticFileName)) {
            for (Long time: statistic.keySet()) {
                String key = time.toString();
                String value = statistic.get(time).toString();
                out.append(key).append(", ").append(value).append("\n");
            }
        } catch (FileNotFoundException e) {
            System.out.println("Файл со статистикой не найден!");
        }
    }

}
