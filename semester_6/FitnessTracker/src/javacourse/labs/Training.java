package javacourse.labs;

public class Training {

    private final Timer timer;
    protected double ccaloriesInHour = 550;

    public Training() {
        timer = new Timer();
    }

    public void start() {
        timer.startThread();
    }

    public boolean isTrain() {
        return timer.isWorking();
    }

    public void finish() {
        timer.finish();
    }

    public double getTrainDuration() {
        return timer.getCurrentDuration();
    }

    public String getTrainName() {
        return "ОФП";
    }

    public double getCalories() {
        return (ccaloriesInHour * 1000.0/3600.0) * getTrainDuration();
    }

}
