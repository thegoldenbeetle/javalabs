package javacourse.labs;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Person person1 = new Person("Ann", "Adamson", 10);
        Person person2 = new Person("Alex", "Brown", 23);
        Person person3 = new Person("Tom", "Brooks", 23);
        Person person4 = new Person("Tom", "Dyson", 46);

        List<Person> persons = new LinkedList<Person>();
        persons.add(person1);
        persons.add(person2);
        persons.add(person3);
        persons.add(person4);

        printList(persons);

        persons.sort(Person.NameComparator);
        printList(persons);

        persons.sort(Person.SurnameComparator);
        printList(persons);

        persons.sort(Person.AgeComparator);
        printList(persons);
    }

    public static void printList(List list) {
        System.out.println(list.toString());
    }
}
