package javacourse.labs;

import java.util.Comparator;

public class Person implements Comparable<Person> {
    private final String name;
    private final String surname;
    private final int age;

    public static Comparator<Person> AgeComparator = (p1, p2) -> (int) (p1.age - p2.age);
    public static Comparator<Person> NameComparator = Comparator.comparing(p -> p.name);
    public static Comparator<Person> SurnameComparator = Comparator.comparing(p -> p.surname);

    public Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    @Override
    public int compareTo(Person person) {
        return this.surname.equals(person.surname) ? this.name.compareTo(person.name) : this.surname.compareTo(person.surname);
    }

    public String toString() {
        return surname + " " + name + " " + age;
    }
}
