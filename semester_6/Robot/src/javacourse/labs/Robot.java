package javacourse.labs;


public class Robot {

    volatile Boolean isLeftStep = false;

    void stepLeft(int stepCount) {
        int step = 0;
        do {
            if (isLeftStep) {
                System.out.println("Left");
                step += 1;
                isLeftStep = false;
            }
        } while (step <= stepCount);
    }

    void stepRight(int stepCount) {
        int step = 0;
        do {
            if (!isLeftStep) {
                System.out.println("Right");
                step += 1;
                isLeftStep = true;
            }
        } while (step <= stepCount);
    }

    void go(int stepCount) {

        Thread leftThread = new Thread(new Runnable() {
            @Override
            public void run() {
                stepLeft(stepCount);
            }
        });
        Thread rightThread = new Thread(new Runnable() {
            @Override
            public void run() {
                stepRight(stepCount);
            }
        });

        leftThread.start();
        rightThread.start();

        try {
            rightThread.join();
            leftThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
