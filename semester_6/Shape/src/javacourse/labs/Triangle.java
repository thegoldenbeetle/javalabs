package javacourse.labs;

public class Triangle implements Shape {

    private final double a;
    private final double b;
    private final double c;

    public Triangle(double a, double b, double c) throws IllegalArgumentException {
        if ((a < b + c) && (b < a + c) && (c < a + b)) {
            this.a = a;
            this.b = b;
            this.c = c;
        } else {
            throw new IllegalArgumentException("Such triangle does not exist!");
        }
    }

    @Override
    public double getArea() {
        double p = (a + b + c) / 2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    public String toString() {
        return "Triangle with a " + a + ", b " + b + ", c " + c;
    }
}
