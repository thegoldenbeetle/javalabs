package javacourse.labs;

public interface Shape {

    public double getArea();

}
