package javacourse.labs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Main {

    public static void main(String[] args) {

        int count = 10;

        ArrayList<Shape> shapes = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            shapes.add(ShapeGenerator.generateShape());
        }

        Shape shapeMaxArea = Collections.max(shapes, Comparator.comparing(Shape::getArea));
        System.out.println(shapeMaxArea);
        System.out.println("Max area: " + shapeMaxArea.getArea());
    }

}
