package javacourse.labs;

import java.util.Random;


public class ShapeGenerator {

    private static Random randomGenerator = new Random();
    public enum ShapeTypes { CIRCLE, RECTANGLE, TRIANGLE };

    public static Shape generateShape() {
        ShapeTypes randomShapeType =  ShapeTypes.values()[getRandomIntInRange(0, 3)];
        Shape shape;
        switch (randomShapeType) {
            case CIRCLE -> shape = generateCircle(0.1, 8.0);
            case RECTANGLE -> shape = generateRectangle(0.17, 17.7);
            case TRIANGLE -> shape = generateTriangle(0.27, 27.0);
            default -> throw new IllegalStateException("Unexpected value");
        }
        return shape;
    }

    public static Circle generateCircle(double minRadius, double maxRadius) {
        return new Circle(getRandomDoubleInRange(minRadius, maxRadius));
    }

    public static Rectangle generateRectangle(double minLength, double maxLength) {
        return new Rectangle(getRandomDoubleInRange(minLength, maxLength), getRandomDoubleInRange(minLength, maxLength));
    }

    public static Triangle generateTriangle(double minLength, double maxLength) {
        double a = getRandomDoubleInRange(minLength, maxLength);
        double b = getRandomDoubleInRange(minLength, maxLength);
        double cosAngle = getRandomDoubleInRange(-1.0, 1.0);
        double c = Math.sqrt(a * a + b * b - 2 * a * b * cosAngle);
        return new Triangle(a, b, c);
    }

    private static int getRandomIntInRange(int min, int max) {
        return randomGenerator.nextInt(max - min) + min;
    }

    private static double getRandomDoubleInRange(double min, double max) {
        return min + (max - min) * randomGenerator.nextDouble();
    }

}
