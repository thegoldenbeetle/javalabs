package javacourse.labs;


public class Main {

    public static void main(String[] args) {
	    Task task = new Task();
	    task.run();
    }

}

class Task {

    public void run() {
        System.out.println("Run with problem:");
        for (int i = 0; i < 10000; i++) {
            runNonSync();
        }
        System.out.println("Run without problem:");
        for (int i = 0; i < 10000; i++) {
            runSync();
        }
    }

    public void runNonSync() {
        Account acc1 = new Account("1", 500);
        Account acc2 = new Account("2", 0);

        Thread transfer1 = new Thread(new Runnable() {
            @Override
            public void run() {
                transferMoneyNonSync(acc1, acc2, 200);
            }
        });
        Thread transfer2 = new Thread(new Runnable() {
            @Override
            public void run() {
                transferMoneyNonSync(acc2, acc1, 100);
            }
        });

        transfer1.start();
        transfer2.start();

        try {
            transfer1.join();
            transfer2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (acc1.getBalance() != 400 || acc2.getBalance() != 100) {
            System.out.println(acc1.getBalance() + " " + acc2.getBalance());
        }
    }

    public void runSync() {
        Account acc1 = new Account("1", 500);
        Account acc2 = new Account("2", 0);

        Thread transfer1 = new Thread(new Runnable() {
            @Override
            public void run() {
                transferMoneySync(acc1, acc2, 200);
            }
        });
        Thread transfer2 = new Thread(new Runnable() {
            @Override
            public void run() {
                transferMoneySync(acc2, acc1, 100);
            }
        });

        transfer1.start();
        transfer2.start();

        try {
            transfer1.join();
            transfer2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (acc1.getBalance() != 400 || acc2.getBalance() != 100) {
            System.out.println(acc1.getBalance() + " " + acc2.getBalance());
        }
    }

    public void transferMoneyNonSync(Account acc1, Account acc2, int summ) {
        acc1.decrement(summ);
        acc2.increment(summ);
    }

    public void transferMoneySync(Account acc1, Account acc2, int summ) {
        String lock1, lock2;
        String id1 = acc1.getId();
        String id2 = acc2.getId();

        if (id1.compareTo(id2) < 0) {
            lock1 = id1;
            lock2 = id2;
        } else {
            lock1 = id2;
            lock2 = id1;
        }

        synchronized(lock1) {
            synchronized(lock2) {
                acc1.decrement(summ);
                acc2.increment(summ);
            }
        }
    }
}

