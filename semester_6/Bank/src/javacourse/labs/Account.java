package javacourse.labs;

public class Account {

    private String id;
    private Integer balance;

    public Account(String id, Integer balance) {
        this.balance = balance;
        this.id = id;
    }

    public Integer getBalance() {
        return balance;
    }
    public String getId() {
        return id;
    }
    public void decrement(int summ) {
        this.balance -= summ;
    }
    public void increment(int summ) {
        this.balance += summ;
    }

}


