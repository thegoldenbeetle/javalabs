package javacourse.labs;

import java.io.*;

public class Main {

    public static void main(String[] args) {
        try {
            String inputString = getStringFromFile("input.txt");
            System.out.printf("String \"%s\" is " +
                    (isPalindrome(inputString) ? "palindrome" : "not palindrome"), inputString
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getStringFromFile(String filename) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename)))
        {
            return reader.readLine();
        }
    }

    public static boolean isPalindrome(String input) {
        String preprocessed = input.toLowerCase().replaceAll("\\s+","");
        int count = 0;
        int middleIndex = preprocessed.length() / 2;
        for (int i = 0; i < middleIndex; ++i) {
            if (preprocessed.charAt(i) == preprocessed.charAt(preprocessed.length() - i - 1)) {
                count += 1;
            }
        }
        return count == middleIndex;
    }

}
