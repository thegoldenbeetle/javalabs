package javacourse.labs;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestsMain {
    @Test
    public void testIsPalindromeTrueEven() {
        boolean result = Main.isPalindrome("abccba");
        Assert.assertTrue(result);
    }

    @Test
    public void testIsPalindromeFalseEven1() {
        boolean result = Main.isPalindrome("abcdef");
        Assert.assertFalse(result);
    }

    @Test
    public void testIsPalindromeFalseEven2() {
        boolean result = Main.isPalindrome("abccea");
        Assert.assertFalse(result);
    }

    @Test
    public void testIsPalindromeTrueOdd() {
        boolean result = Main.isPalindrome("abcdedcba");
        Assert.assertTrue(result);
    }

    @Test
    public void testisPalindromeFalseOdd() {
        boolean result = Main.isPalindrome("abcdedcmq");
        Assert.assertFalse(result);
    }

    @Test
    public void testisPalindromeEmptyString() {
        boolean result = Main.isPalindrome("");
        Assert.assertTrue(result);
    }

    @Test
    public void testisPalindrome1symbol() {
        boolean result = Main.isPalindrome("a");
        Assert.assertTrue(result);
    }

    @Test
    public void testisPalindromeStringTrue() {
        boolean result = Main.isPalindrome("Was it a car or a cat I saw");
        Assert.assertTrue(result);
    }

    @Test
    public void testisPalindromeStringFalse() {
        boolean result = Main.isPalindrome("Was it a car or a dog I saw");
        Assert.assertFalse(result);
    }

}
